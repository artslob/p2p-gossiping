use std::collections::BTreeSet;
use std::net::{Ipv4Addr, SocketAddr};

pub struct Peer {
    pub port: Port,
}

impl Peer {
    pub fn addresses<'a>(
        &'a self,
        nodes: &'a BTreeSet<Port>,
    ) -> impl Iterator<Item = SocketAddr> + '_ {
        nodes
            .iter()
            .copied()
            .filter(|port| *port != self.port)
            .map(|port| port.address())
    }
}

#[derive(
    Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, serde::Serialize, serde::Deserialize,
)]
pub struct Port(u16);

impl Port {
    pub fn address(&self) -> SocketAddr {
        SocketAddr::from((Ipv4Addr::LOCALHOST, self.0))
    }
}

impl std::str::FromStr for Port {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(u16::from_str(s)?))
    }
}
