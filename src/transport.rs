use std::net::TcpStream;

pub trait TcpTransport {
    fn send<RQ: serde::Serialize + ?Sized>(&self, request: &RQ) -> anyhow::Result<()>;
    fn receive<RS: serde::de::DeserializeOwned>(&self) -> anyhow::Result<RS>;
}

impl TcpTransport for TcpStream {
    fn send<RQ: serde::Serialize + ?Sized>(&self, request: &RQ) -> anyhow::Result<()> {
        bincode::serialize_into(self, request).map_err(Into::into)
    }

    fn receive<RS: serde::de::DeserializeOwned>(&self) -> anyhow::Result<RS> {
        bincode::deserialize_from(self).map_err(Into::into)
    }
}
