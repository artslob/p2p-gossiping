#![feature(map_first_last)]
use crate::api::{Action, ConnectResponse, Request};
use crate::peer::{Peer, Port};
use crate::transport::TcpTransport;
use env_logger::Env;
use log::{debug, error, info};
use signal_hook::consts::SIGINT;
use signal_hook::iterator::Signals;
use std::collections::BTreeSet;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

mod api;
mod cli;
mod peer;
mod transport;

fn main() -> anyhow::Result<()> {
    env_logger::Builder::new()
        .parse_env(Env::new().default_filter_or("info"))
        .init();

    let cli = cli::Args::parse();
    let peer: &'static Peer = Box::leak(Box::new(cli.peer()));

    let server_address = peer.port.address();
    info!("My address is {}", server_address);

    let nodes = match cli.connect {
        None => common_macros::b_tree_set!(peer.port),
        Some(connect_to) => connect(peer, connect_to),
    };

    let nodes = Arc::new(Mutex::new(nodes));

    set_exit_hook(peer, Arc::clone(&nodes))?;

    {
        let nodes = Arc::clone(&nodes);
        thread::spawn(move || ping(peer, cli.period, nodes));
    }

    for stream in TcpListener::bind(server_address)?.incoming() {
        match stream {
            Ok(stream) => {
                debug!("New connection: {:?}", stream.peer_addr());
                let nodes = Arc::clone(&nodes);
                rayon::spawn(move || {
                    if let Err(err) = handle_connection(stream, nodes) {
                        error!("Error while connecting: {}", err);
                    }
                });
            }
            Err(e) => error!("Error while receiving message: {}", e),
        }
    }

    Ok(())
}

fn handle_connection(stream: TcpStream, nodes: Arc<Mutex<BTreeSet<Port>>>) -> anyhow::Result<()> {
    let request: Request = stream.receive()?;
    match request.action {
        Action::Connect => {
            let connect_response = {
                // nested code block allows decrease locking time
                let mut nodes = nodes.lock().unwrap();
                nodes.insert(request.sender);
                ConnectResponse(nodes.clone())
            };
            stream.send(&connect_response)?;
            info!(
                "Connected {:?} peer. Current nodes: {:?}",
                request.sender, &connect_response.0
            );
        }
        Action::Ping => {
            info!("Received message from '{:?}'", request.sender.address());
        }
        Action::Quit => {
            let mut nodes = nodes.lock().unwrap();
            nodes.remove(&request.sender);
            info!(
                "Disconnected {:?} peer. Current nodes: {:?}",
                request.sender, &nodes
            );
        }
    }
    Ok(())
}

/// Join peer network. Collects all nodes.
fn connect(peer: &Peer, connect_to: Port) -> BTreeSet<Port> {
    let mut nodes = common_macros::b_tree_set!(peer.port);
    let mut check_required = common_macros::b_tree_set!(connect_to);

    while let Some(next_port) = check_required.pop_first() {
        // can be improved by moving connections to threads
        if let Err(err) = inner_connect(peer, next_port, &mut nodes, &mut check_required) {
            error!("Error while connecting: {}", err);
        }
    }

    info!("Connected to: {:?}", nodes);

    nodes
}

fn inner_connect(
    peer: &Peer,
    next_port: Port,
    nodes: &mut BTreeSet<Port>,
    check_required: &mut BTreeSet<Port>,
) -> anyhow::Result<()> {
    debug!("next port in queue is {:?}", next_port);

    let stream = TcpStream::connect(next_port.address())?;

    stream.send(&Request::new(peer, Action::Connect))?;

    let response: ConnectResponse = stream.receive()?;
    debug!("got response {:?}", response);

    nodes.insert(next_port);
    check_required.extend(response.0.iter().filter(|number| !nodes.contains(number)));
    debug!("current check required {:?}", check_required);

    Ok(())
}

/// Send ping message to all others peers
fn ping(peer: &'static Peer, period: u8, nodes: Arc<Mutex<BTreeSet<Port>>>) {
    loop {
        std::thread::sleep(Duration::from_secs(period as u64));

        let receivers = { nodes.lock().unwrap().clone() };

        for address in peer.addresses(&receivers) {
            rayon::spawn(move || {
                if let Err(err) = inner_ping(peer, address) {
                    error!("Error while ping: {}", err);
                }
            });
        }
    }
}

fn inner_ping(peer: &Peer, address: SocketAddr) -> anyhow::Result<()> {
    let stream = match TcpStream::connect(address) {
        Ok(value) => value,
        Err(_) => {
            // we can also delete node from receivers if we want to
            debug!("could not send message to {}", address);
            return Ok(());
        }
    };
    stream.send(&Request::new(peer, Action::Ping))?;
    info!("Sending message to '{:?}'", address);
    Ok(())
}

fn set_exit_hook(peer: &'static Peer, nodes: Arc<Mutex<BTreeSet<Port>>>) -> anyhow::Result<()> {
    let mut signals = Signals::new(&[SIGINT])?;
    thread::spawn(move || {
        signals.forever().next();
        let nodes = nodes.lock().unwrap();
        quit(peer, &nodes);
        std::process::exit(0);
    });
    Ok(())
}

/// Quits peer network
fn quit(peer: &Peer, nodes: &BTreeSet<Port>) {
    for address in peer.addresses(nodes) {
        let stream = match TcpStream::connect(address) {
            Ok(value) => value,
            Err(e) => {
                error!("Error while sending quit message: {}", e);
                continue;
            }
        };
        let _ = stream.send(&Request::new(peer, Action::Quit));
        info!("Quit message to '{:?}'", address);
    }
}
