use crate::peer::{Peer, Port};
use clap::Parser;

#[derive(Parser)]
#[clap(version)]
/// Simple p2p gossiping application in Rust
pub struct Args {
    /// Which port to use to receive messages
    #[clap(long)]
    port: Port,
    /// How often send messages; in seconds
    #[clap(long, default_value_t = 1)]
    pub period: u8,
    /// Port of another peer which should be connected
    #[clap(long)]
    pub connect: Option<Port>,
}

impl Args {
    pub fn peer(&self) -> Peer {
        Peer { port: self.port }
    }
}

impl Args {
    pub fn parse() -> Self {
        <Self as Parser>::parse()
    }
}
