use crate::peer::Port;
use crate::Peer;
use std::collections::BTreeSet;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Request {
    pub sender: Port,
    pub action: Action,
}

impl Request {
    pub fn new(peer: &Peer, action: Action) -> Self {
        Self {
            sender: peer.port,
            action,
        }
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum Action {
    Connect,
    Ping,
    Quit,
}

// TODO maybe use Cow
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ConnectResponse(pub BTreeSet<Port>);
