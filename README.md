# p2p-gossiping

Simple p2p gossiping application in Rust. The peer have a cli interface to start it and connect
itself to the other peers. Once connected, the peer sends a random gossip message to all the other
peers every N seconds. The messaging period is specifiable in the command line. When a peer
receives a message from the other peers, it prints it in the console.

## Implementation details
1. For simplicity all addresses are constrained to localhost and peers are identified by their
   port numbers.
2. For simplicity peer connects to each node in peer network when first joining network. This
   allows to collect all node addresses even in concurrent environment. Alternative way is connecting
   to only 1 node during network joining, but then collect missing peers in ping messages.
3. New peers can connect any peer to join the network.
4. Set logging level using env var like this: `RUST_LOG=debug cargo run ...`

## Example usage
Start program as 3 different processes:
```bash
# terminal #1
cargo r -- --port 8000 --period 4
# terminal #2
cargo r -- --port 8001 --connect 8000 --period 5
# terminal #3
cargo r -- --port 8002 --connect 8001 --period 3
```
Example output (removed module from log for brevity):
Terminal #1:
```
[2022-04-07T19:45:10Z INFO] My address is 127.0.0.1:8000
[2022-04-07T19:45:13Z INFO] Connected Port(8001) peer. Current nodes: {Port(8000), Port(8001)}
[2022-04-07T19:45:14Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:16Z INFO] Connected Port(8002) peer. Current nodes: {Port(8000), Port(8001), Port(8002)}
[2022-04-07T19:45:18Z INFO] Received message from '127.0.0.1:8001'
[2022-04-07T19:45:18Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:18Z INFO] Sending message to '127.0.0.1:8002'
[2022-04-07T19:45:19Z INFO] Received message from '127.0.0.1:8002'
[2022-04-07T19:45:22Z INFO] Received message from '127.0.0.1:8002'
[2022-04-07T19:45:22Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:22Z INFO] Sending message to '127.0.0.1:8002'
[2022-04-07T19:45:23Z INFO] Received message from '127.0.0.1:8001'
[2022-04-07T19:45:25Z INFO] Received message from '127.0.0.1:8002'
^C
[2022-04-07T19:45:25Z INFO] Quit message to '127.0.0.1:8001'
[2022-04-07T19:45:25Z INFO] Quit message to '127.0.0.1:8002'
```
Terminal #2:
```
[2022-04-07T19:45:13Z INFO] My address is 127.0.0.1:8001
[2022-04-07T19:45:13Z INFO] Connected to: {Port(8000), Port(8001)}
[2022-04-07T19:45:14Z INFO] Received message from '127.0.0.1:8000'
[2022-04-07T19:45:16Z INFO] Connected Port(8002) peer. Current nodes: {Port(8000), Port(8001), Port(8002)}
[2022-04-07T19:45:18Z INFO] Sending message to '127.0.0.1:8000'
[2022-04-07T19:45:18Z INFO] Sending message to '127.0.0.1:8002'
[2022-04-07T19:45:18Z INFO] Received message from '127.0.0.1:8000'
[2022-04-07T19:45:19Z INFO] Received message from '127.0.0.1:8002'
[2022-04-07T19:45:22Z INFO] Received message from '127.0.0.1:8002'
[2022-04-07T19:45:22Z INFO] Received message from '127.0.0.1:8000'
[2022-04-07T19:45:23Z INFO] Sending message to '127.0.0.1:8000'
[2022-04-07T19:45:23Z INFO] Sending message to '127.0.0.1:8002'
[2022-04-07T19:45:25Z INFO] Received message from '127.0.0.1:8002'
[2022-04-07T19:45:25Z INFO] Disconnected Port(8000) peer. Current nodes: {Port(8001), Port(8002)}
[2022-04-07T19:45:28Z INFO] Received message from '127.0.0.1:8002'
[2022-04-07T19:45:28Z INFO] Sending message to '127.0.0.1:8002'
^C
[2022-04-07T19:45:29Z INFO] Quit message to '127.0.0.1:8002'
```
Terminal #3:
```
[2022-04-07T19:45:16Z INFO] My address is 127.0.0.1:8002
[2022-04-07T19:45:16Z INFO] Connected to: {Port(8000), Port(8001), Port(8002)}
[2022-04-07T19:45:18Z INFO] Received message from '127.0.0.1:8001'
[2022-04-07T19:45:18Z INFO] Received message from '127.0.0.1:8000'
[2022-04-07T19:45:19Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:19Z INFO] Sending message to '127.0.0.1:8000'
[2022-04-07T19:45:22Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:22Z INFO] Sending message to '127.0.0.1:8000'
[2022-04-07T19:45:22Z INFO] Received message from '127.0.0.1:8000'
[2022-04-07T19:45:23Z INFO] Received message from '127.0.0.1:8001'
[2022-04-07T19:45:25Z INFO] Sending message to '127.0.0.1:8000'
[2022-04-07T19:45:25Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:25Z INFO] Disconnected Port(8000) peer. Current nodes: {Port(8001), Port(8002)}
[2022-04-07T19:45:28Z INFO] Sending message to '127.0.0.1:8001'
[2022-04-07T19:45:28Z INFO] Received message from '127.0.0.1:8001'
[2022-04-07T19:45:29Z INFO] Disconnected Port(8001) peer. Current nodes: {Port(8002)}
^C
```
